<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%item}}`.
 */
class m191123_075958_create_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%item}}', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'name' => $this->text(),
            'path' => $this->text(),
            'price' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%item}}');
    }
}
