<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%item_img}}`.
 */
class m191123_201335_create_item_img_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%item_img}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'path' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%item_img}}');
    }
}
