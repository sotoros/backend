<?php

use yii\db\Migration;

/**
 * Class m191123_161434_alter_transaction_table
 */
class m191123_161434_alter_transaction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('transaction', 'user_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191123_161434_alter_transaction_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191123_161434_alter_transaction_table cannot be reverted.\n";

        return false;
    }
    */
}
