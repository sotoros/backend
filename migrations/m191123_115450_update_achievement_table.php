<?php

use yii\db\Migration;

/**
 * Class m191123_115450_update_achievement_table
 */
class m191123_115450_update_achievement_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('achievement', 'award', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191123_115450_update_achievement_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191123_115450_update_achievement_table cannot be reverted.\n";

        return false;
    }
    */
}
