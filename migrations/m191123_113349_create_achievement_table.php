<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%achievement}}`.
 */
class m191123_113349_create_achievement_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%achievement}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'product' => $this->text(),
            'needed' => $this->integer(),
            'bought' => $this->integer(),
            'complited' => $this->integer(),
            'end_date' => $this->date(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%achievement}}');
    }
}
