<?php

use yii\db\Migration;

/**
 * Class m191123_195522_alter_item_table
 */
class m191123_195522_alter_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('item', 'path');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191123_195522_alter_item_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191123_195522_alter_item_table cannot be reverted.\n";

        return false;
    }
    */
}
