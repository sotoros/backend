<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%link_item}}`.
 */
class m191123_194942_create_link_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%link_item}}', [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer(),
            'head' => $this->string(),
            'left_arm' => $this->string(),
            'right_arm' => $this->string(),
            'body' => $this->string(),
            'left_leg' => $this->string(),
            'right_leg' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%link_item}}');
    }
}
