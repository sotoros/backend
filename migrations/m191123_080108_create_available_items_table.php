<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%available_items}}`.
 */
class m191123_080108_create_available_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%available_items}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'item_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%available_items}}');
    }
}
