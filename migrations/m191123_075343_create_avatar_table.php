<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%avatar}}`.
 */
class m191123_075343_create_avatar_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%avatar}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'head' => $this->integer(),
            'left_arm' => $this->integer(),
            'right_arm' => $this->integer(),
            'body' => $this->integer(),
            'left_leg' => $this->integer(),
            'right_leg' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%avatar}}');
    }
}
