<?php

use yii\db\Migration;

/**
 * Class m191122_225304_add_link_transaction_product
 */
class m191122_225304_add_link_transaction_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addForeignKey(
			'fk-product-id_trans',
			'product',
			'id_trans',
			'transaction',
			'id'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropForeignKey('fk-product-transaction_id', 'product');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191122_225304_add_link_transaction_product cannot be reverted.\n";

        return false;
    }
    */
}
