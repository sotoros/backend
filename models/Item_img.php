<?php

namespace micro\models;

use Yii;


class Item_img extends \yii\db\ActiveRecord
{

    public $img_path;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item_img';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'path'], 'string'],
        ];
    }
}
