<?php

namespace micro\models;

use Yii;

use micro\models\Product;
use micro\models\Item_img;
/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property string|null $date
 * @property int|null $amount
 * @property string|null $description
 *
 * @property Product[] $products
 */
class Transaction extends \yii\db\ActiveRecord
{

    public $img_path;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['amount', 'user_id'], 'integer'],
            [['description'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'amount' => 'Amount',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id_trans' => 'id']);
    }

    public function path()
    {
        $data = [
            'Лента' => 'img/icons/logo_lenta.png',
            'КИНОМАКС' => 'img/icons/kinomax.png',
            'MACDONALDS' => 'img/icons/md.png',
            'Парихмахерская' => 'img/icons/barber.png',
            'СПОРТМАСТЕР' => 'img/icons/sm.png',
            'Зарплата' => null,
            'OSTIN' => 'img/icons/ostin.jpg',
            'Клуб' => null,
            'Кассандра' => null,
            
        ];

        if(isset($data[$this->description]))
            $this->img_path = $data[$this->description];
        else 
            $this->img_path = null;
        //$this->img_path = Item_img::find()->andWhere(['name' => $this->description])->one()['path'];

        return true;
    }
}
