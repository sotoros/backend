<?php

namespace micro\models;

use Yii;


class Link extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'link_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['item_id', 'integer'],
            [['head', 'left_arm', 'right_arm', 'body', 'left_leg', 'right_leg'], 'string'],
        ];
    }
}
