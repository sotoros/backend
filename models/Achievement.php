<?php

namespace micro\models;

use Yii;

class Achievement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'achievement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'needed', 'bought', 'award', 'complited'], 'integer'],
            ['product', 'string'],
            ['end_date', 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }
}
