<?php

namespace micro\models;

use Yii;

class Item extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'price'], 'integer'],
            [['name', 'path'], 'string']
        ];
    }
}
