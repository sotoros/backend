<?php

namespace micro\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\base\Model;

class UserReg extends Model
{ 
    public $name;
    public $login;
    public $password;
    public $password_repeat;

    public function rules()
    {
        return [
            [[ 'name', 'login', 'password', 'password_repeat'], 'required', 'message' => 'Поля не должны быть пустыми.'],
            [['name', 'login', 'password', 'password_repeat'], 'trim'],
            ['password', 'compare'],
        ];
    }
    
    public function save()
    {
        $model = new User();

        $password = password_hash($this->password, PASSWORD_DEFAULT);

        $model->name = $this->name;   
        $model->login = $this->login;   
        $model->password = $password;   
        
        return $model->save();
    }

}