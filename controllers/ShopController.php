<?php

namespace micro\controllers;

use yii;
use micro\models\Achievement;
use micro\models\Transaction;
use micro\models\Product;
use micro\models\Item;
use micro\models\Available;
use micro\models\Avatar;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;

/**
 * Работа с транзакциями
 *
 * Class ShopController
 * @package micro\controllers
 */
class ShopController extends ActiveController
{
    public $modelClass = 'micro\models\Transaction';

    public function behaviors()
    {
        // удаляем rateLimiter, требуется для аутентификации пользователя
        $behaviors = parent::behaviors();

        unset($behaviors['rateLimiter']);

        // Возвращает результаты экшенов в формате JSON
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        // OAuth 2.0
        $behaviors['authenticator'] = ['class' => HttpBearerAuth::className()];

        return $behaviors;
    }

    public function actionBuy($id)
    {
        if(Available::find()->where(['item_id' => $id, 'user_id' => Yii::$app->user->id])->exists())
            return 'exist';

        $balance = Yii::$app->user->identity->balance;

        $item = Item::find()->where(['id'=> $id])->one();

        if($item['price'] > $balance)
            return 'not_enough';

        $user = Yii::$app->user->identity;
        $user->balance -= $item['price'];
        $user->save();
        $model = new Available();
        $model->user_id = Yii::$app->user->id;
        $model->item_id = $id;
        if($model->save())
            return true;
        else 
            return $model->errors;


    }

    public function actionGoods()
    {

        $available = ArrayHelper::getColumn(Available::find()->select(['item_id'])->where(['user_id' => Yii::$app->user->id])->asArray()->all(), 'item_id');
        $model = Item::find()
            ->orderBy('type')
            ->where(['NOT IN','id',$available])
            ->asArray()
            ->all();;

        return $model;
    }

    public function actionBought()
    {
        $available = ArrayHelper::getColumn(Available::find()->select(['item_id'])->where(['user_id' => Yii::$app->user->id])->asArray()->all(), 'item_id');
        $model = Item::find()
            ->orderBy('type')
            ->where(['IN','id',$available])
            ->asArray()
            ->all();;

        return $model;
    }

    public function actionEquip($id)
    {
        $avatar = Avatar::find()->where(['user_id' => 2])->one();

        $human = [];
        $human[0] = 'head';
        $human[1] = 'left_arm';
        $human[2] = 'right_arm';
        $human[4] = 'left_leg';
        $human[3] = 'body';
        $human[5] = 'right_leg';
        $human[6] = 'right_arm';

        $item = Item::find()->where(['id'=> $id])->one();
        if($avatar->{$human[$item['type']]} == $item['id'])
            $avatar->{$human[$item['type']]} = null;
        else 
            $avatar->{$human[$item['type']]} = $item['id'];
        $avatar->save();

        return true;
    }

}

