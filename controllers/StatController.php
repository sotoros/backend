<?php
/**
 * Created by PhpStorm.
 * User: sotoros
 * Date: 24.11.2019
 * Time: 4:56
 */

namespace micro\controllers;

use yii;
use micro\models\Achievement;
use micro\models\Transaction;
use micro\models\Product;
use yii\rest\Controller;
use yii\web\Response;
use yii\filters\auth\HttpBearerAuth;

/**
 * Работа с транзакциями
 *
 * Class AchievementController
 * @package micro\controllers
 */
class StatController extends Controller
{
	public function behaviors()
	{
		// удаляем rateLimiter, требуется для аутентификации пользователя
		$behaviors = parent::behaviors();

		unset($behaviors['rateLimiter']);

		// Возвращает результаты экшенов в формате JSON
		$behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
		// OAuth 2.0
		$behaviors['authenticator'] = ['class' => HttpBearerAuth::className()];

		return $behaviors;
	}

	public function actionGet() {
		$intelGood = Transaction::find()
			->where(['description' => 'Кассандра'])
			->orWhere(['description' => 'Театр'])
			->andWhere(['user_id' => Yii::$app->user->identity->id])
			->sum('amount');

		$intelBad = Transaction::find()
			->where(['description' => 'Клуб'])
			->andWhere(['user_id' => Yii::$app->user->identity->id])
			->sum('amount');

		$intel = ceil(($intelGood / ($intelGood + $intelBad)) * 100);

		$powerGood = Transaction::find()
			->where(['description' => 'СПОРТМАСТЕР'])
			->orWhere(['description' => 'Well Fit'])
			->andWhere(['user_id' => Yii::$app->user->identity->id])
			->sum('amount');

		$powerBad = Transaction::find()
			->where(['description' => 'MACDONALDS'])
			->andWhere(['user_id' => Yii::$app->user->identity->id])
			->sum('amount');

		$power = ceil(($powerGood / ($powerGood + $powerBad)) * 100);

		$del = Transaction::find()
			->andWhere(['user_id' => Yii::$app->user->identity->id])
			->sum('amount');

		$get = Transaction::find()
			->where(['description' => 'Зарплата'])
			->andWhere(['user_id' => Yii::$app->user->identity->id])
			->sum('amount');

		$money = ceil(( abs($del) / abs($get)) * 100);

		$result = new \stdClass();

		$result->intel = $intel;
		$result->power = $power;
		$result->money = $money;
		$result->get = $get;
		$result->del = $del;

		return $result;
	}
}

