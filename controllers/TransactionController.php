<?php

namespace micro\controllers;

use micro\models\Product;
use micro\models\Transaction;
use yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Работа с транзакциями
 *
 * Class TransactionController
 * @package micro\controllers
 */
class TransactionController extends ActiveController
{
	public $modelClass = 'micro\models\Transaction';

	public function behaviors()
	{
		// удаляем rateLimiter, требуется для аутентификации пользователя
		$behaviors = parent::behaviors();

		unset($behaviors['rateLimiter']);

		// Возвращает результаты экшенов в формате JSON
		$behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
		// OAuth 2.0
		$behaviors['authenticator'] = ['class' => HttpBearerAuth::className()];

		return $behaviors;
	}

	/**
	 * Получение последней транзации
	 *
	 * @return mixed
	 */
	public function actionLast()
	{
		$transaction = Transaction::find()->orderBy(['id' => SORT_DESC])->andWhere(['user_id' => Yii::$app->user->id])->one();

		$result = [];

		$transaction->path();

		$result['id'] = $transaction->id;
		$result['date'] = $transaction->date;
		$result['amount'] = $transaction->amount;
		$result['description'] = $transaction->description;
		$result['path'] = $transaction->img_path;

		return $result;
	}

	/**
	 * Получение всех транзакций
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
	public function actionAll()
	{
		$status = \Yii::$app->request->post('status');

		$start = $end = null;

		if ($status == 'week') {
			$previous_week = strtotime("now");

			$start_week = strtotime("-1 week", $previous_week);
			$end_week = $previous_week;

			$start = date("Y-m-d H:i:s", $start_week);
			$end = date("Y-m-d H:i:s", $end_week);
		} else if ($status == 'month') {
			$previous_week = strtotime("now");

			$start_week = strtotime("-1 month", $previous_week);
			$end_week = $previous_week;

			$start = date("Y-m-d H:i:s", $start_week);
			$end = date("Y-m-d H:i:s", $end_week);
		} else if ($status == 'prev') {
			$previous_week = strtotime("now");

			$start_week = strtotime("-2 month", $previous_week);
			$end_week = strtotime("-1 month", $previous_week);

			$start = date("Y-m-d H:i:s", $start_week);
			$end = date("Y-m-d H:i:s", $end_week);
		}

		$transaction = Transaction::find()
			->where(['>', 'date', $start])
			->andWhere(['<', 'date', $end])
			->andWhere(['user_id' => 2])
			->orderBy(['id' => SORT_DESC])
			->all();

		$result = [];

		foreach ($transaction as $key => $s) {
			$transaction[$key]->path();
			$result[$key]['date'] = $transaction[$key]->date;
			$result[$key]['amount'] = $transaction[$key]->amount;
			$result[$key]['description'] = $transaction[$key]->description;
			$result[$key]['path'] = $transaction[$key]->img_path;
		}

		return $result;
	}

	/**
	 * Получение всех продуктов по id транзакции
	 *
	 * @param $id
	 * @return mixed
	 */
	public function actionProducts($id)
	{
		$t_id = Transaction::find()->where(['id' => $id])->one()['id'];


		return Product::find()->where(['id_trans' => $t_id])->all();
	}

public function actionGenerate($user_id)
	{
		date_default_timezone_set('UTC');

		$date = '2019-09-23';
		$end_date = '2019-11-23';
		$result = [];
		$k = 0;
		$product = [];

		while (strtotime($date) <= strtotime($end_date)) {
			$result[$k]['date'] = $date;
			$amount = rand(1, 3);
			for ($i = 1; $i <= $amount; $i++) {
				$product = [];
				$rngGod = rand(0, 53);
				$result[$k][$i]['date'] = date("Y-m-d H:i:s", strtotime("+" . rand(0, 24) . " hour + " . rand(0, 60) . " minute +" . rand(0, 60) . "second", strtotime($date)));

				if ($rngGod < 20) {
					$result[$k][$i]['shop'] = 'Лента';

					$lent_list = [
						['amount' => -30, 'name' => 'Хлеб', 'type' => 'Штук'],
						['amount' => -50, 'name' => 'Молоко', 'type' => 'Литр'],
						['amount' => -250, 'name' => 'Мясо', 'type' => 'Кг'],
						['amount' => -50, 'name' => 'Вода', 'type' => 'Литр'],
						['amount' => -100, 'name' => 'Кола', 'type' => 'Литр']

					];

					$product_amount = rand(1, 5);

					for ($z = 1; $z <= $product_amount; $z++) {
						$product[$z] = $lent_list[rand(0, count($lent_list) - 1)];
					}
				} elseif (($rngGod < 23) && ($rngGod >= 20)) {
					$result[$k][$i]['shop'] = 'OSTIN';

					$ostin_list = [
						['amount' => -3000, 'name' => 'Куртка', 'type' => 'SHTUK'],
						['amount' => -1000, 'name' => 'Шорты', 'type' => 'SHTUK'],
						['amount' => -1500, 'name' => 'Рубашка', 'type' => 'SHTUK'],
						['amount' => -500, 'name' => 'Шапка', 'type' => 'SHTUK'],
					];

					$product_amount = rand(1, 5);

					for ($z = 1; $z <= $product_amount; $z++) {
						$product[$z] = $ostin_list[rand(0, count($ostin_list) - 1)];
					}
				} elseif (($rngGod < 30) && ($rngGod >= 23)) {
					$result[$k][$i]['shop'] = 'MACDONALDS';

					$MD_list = [
						['amount' => -300, 'name' => 'Бупгер', 'type' => 'SHTUK'],
						['amount' => -150, 'name' => 'Кола', 'type' => 'LITR'],
						['amount' => -450, 'name' => 'Мега Бургер', 'type' => 'SHTUK']
					];

					$product_amount = rand(1, 3);

					for ($z = 1; $z <= $product_amount; $z++) {
						$product[$z] = $MD_list[rand(0, count($MD_list) - 1)];
					}
				} elseif (($rngGod < 34) && ($rngGod >= 30)) {
					$result[$k][$i]['shop'] = 'КИНОМАКС';
					$tikets_amount = rand(0, 100);
					if ($tikets_amount < 80) {
						$product[0]['name'] = 'Билет в кино';
						$product[0]['type'] = 'SHTUK';
						$product[0]['amount'] = -300 - 50 * rand(1, 5);
					} else {
						$product[0]['name'] = 'Билет в кино';
						$product[0]['type'] = 'SHTUK';
						$product[0]['amount'] = -300 - 50 * rand(1, 5);

						$product[1]['name'] = 'Билет в кино';
						$product[1]['type'] = 'SHTUK';
						$product[1]['amount'] = -300 - 50 * rand(1, 5);
					}
				} elseif (($rngGod < 37) && ($rngGod >= 34)) {
					$result[$k][$i]['shop'] = 'СПОРТМАСТЕР';

					$sport_list = [
						['amount' => -3500, 'name' => 'Куртка спортивная', 'type' => 'SHTUK'],
						['amount' => -2000, 'name' => 'Шорты спортивные', 'type' => 'SHTUK'],
						['amount' => -1500, 'name' => 'Рубашка спортивная', 'type' => 'SHTUK'],
						['amount' => -500, 'name' => 'Шапка спортивная', 'type' => 'SHTUK'],
					];

					$product_amount = rand(1, 4);

					for ($z = 1; $z <= $product_amount; $z++) {
						$product[$z] = $sport_list[rand(0, count($sport_list) - 1)];
					}
				} elseif (($rngGod < 37) && ($rngGod >= 34)) {
					$result[$k][$i]['shop'] = 'СПОРТМАСТЕР';

					$sport_list = [
						['amount' => -3500, 'name' => 'Куртка спортивная', 'type' => 'SHTUK'],
						['amount' => -2000, 'name' => 'Шорты спортивные', 'type' => 'SHTUK'],
						['amount' => -1500, 'name' => 'Рубашка спортивная', 'type' => 'SHTUK'],
						['amount' => -500, 'name' => 'Шапка спортивная', 'type' => 'SHTUK'],
					];

					$product_amount = rand(1, 4);

					for ($z = 1; $z <= $product_amount; $z++) {
						$product[$z] = $sport_list[rand(0, count($sport_list) - 1)];
					}
				} elseif (($rngGod < 44) && ($rngGod >= 37)) {
					$result[$k][$i]['shop'] = 'Well Fit';

					$sport_list = [
						['amount' => -350, 'name' => 'Посещение бассейна', 'type' => 'SHTUK'],
						['amount' => -250, 'name' => 'Посещение тренажерного зала', 'type' => 'SHTUK'],
						['amount' => -500, 'name' => 'Посещение секции', 'type' => 'SHTUK'],
					];

					$product_amount = rand(1, 3);

					for ($z = 1; $z <= $product_amount; $z++) {
						$product[$z] = $sport_list[rand(0, count($sport_list) - 1)];
					}
				} elseif (($rngGod < 45) && ($rngGod >= 44)) {
					$result[$k][$i]['shop'] = 'Театр';

					$sport_list = [
						['amount' => -250, 'name' => 'Билет на представление', 'type' => 'SHTUK'],
						['amount' => -250, 'name' => 'Билет на представление', 'type' => 'SHTUK'],
					];

					$product_amount = rand(1, 2);

					for ($z = 1; $z <= $product_amount; $z++) {
						$product[$z] = $sport_list[rand(0, count($sport_list) - 1)];
					}
				} elseif (($rngGod < 50) && ($rngGod >= 45)) {
					$result[$k][$i]['shop'] = 'Кассандра';

					$sport_list = [
						['amount' => -375, 'name' => 'Покупка книги', 'type' => 'SHTUK'],
						['amount' => -1200, 'name' => 'Покупка книги', 'type' => 'SHTUK'],
						['amount' => -200, 'name' => 'Покупка книги', 'type' => 'SHTUK'],
						['amount' => -270, 'name' => 'Покупка книги', 'type' => 'SHTUK'],
					];

					$product_amount = rand(1, 4);

					for ($z = 1; $z <= $product_amount; $z++) {
						$product[$z] = $sport_list[rand(0, count($sport_list) - 1)];
					}
				} elseif (($rngGod <= 53) && ($rngGod >= 50)) {
					$result[$k][$i]['shop'] = 'Клуб';
 
					$sport_list = [
						['amount' => -375, 'name' => 'Оплата прохода', 'type' => 'SHTUK'],
						['amount' => -500, 'name' => 'Покупка алкоголя', 'type' => 'SHTUK'],
						['amount' => -200, 'name' => 'Покупка сигарет', 'type' => 'SHTUK'],
					];

					$product_amount = rand(1, 3);

					for ($z = 1; $z <= $product_amount; $z++) {
						$product[$z] = $sport_list[rand(0, count($sport_list) - 1)];
					}
				}

				$transaction_amount = 0;

				foreach ($product as $key => $v) {
					$transaction_amount += $v['amount'];
				}

				$model = new Transaction();

				$model->date = $result[$k][$i]['date'];
				$model->amount = $transaction_amount;
				$model->description = $result[$k][$i]['shop'];
				$model->user_id = $user_id;

				$model->save();

				foreach ($product as $key => $p) {
					$product_model = new Product();
					$product_model->id_trans = $model->id;
					$product_model->amount = $p['amount'];
					$product_model->name = $p['name'];
					$product_model->type = $p['type'];
					$product_model->save();
				}


			}

			if(intval(date('d', strtotime($date))) == 5 || intval(date('d', strtotime($date))) == 20)
			{
				$model = new Transaction();

				$model->date = date("Y-m-d", strtotime("+0 day", strtotime($date)));
				$model->amount = 50000;
				$model->description = 'Зарплата';
				$model->user_id = $user_id;
				$model->save();


				$product_model = new Product();
				$product_model->id_trans = $model->id;
				$product_model->amount = 50000;
				$product_model->name = 'Зарплата';
				$product_model->type = 0;
				$product_model->save();
			}

			$k++;
			$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
 
		}

		return $result;
	}


}
