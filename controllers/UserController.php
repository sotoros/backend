<?php

namespace micro\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\filters\auth\HttpBearerAuth;
use micro\models\Avatar;
use micro\models\User;
use micro\models\Link;

// RESTfull контроллер для модели User
class UserController extends ActiveController
{
    public $modelClass = 'micro\models\User';

    public function behaviors()
    {
        // удаляем rateLimiter, требуется для аутентификации пользователя
        $behaviors = parent::behaviors();

        unset($behaviors['rateLimiter']);

        // Возвращает результаты экшенов в формате JSON  
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON; 
        // OAuth 2.0
        $behaviors['authenticator'] = ['class' => HttpBearerAuth::className()];

        return $behaviors;
    }

    public function actionAvatar()
    {
        $avatar = Avatar::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $avatar;
    }

    public function actionMe()
    {
        return Yii::$app->user->identity;
    }

    public function actionSprites()
    {
        $set = Avatar::find()->where(['user_id' => Yii::$app->user->id])->one();

        $result = [];
        //return $set;
        $return_result = [];
        if(isset($set->head))
        {
            $tmp = Link::find()->where(['item_id' => $set->head])->asArray()->one();
            $result['head'] = $tmp;
            $return_result['hair'] = $tmp['head'];
        }

        if(isset($set->body))
        {
            $tmp = Link::find()->where(['item_id' => $set->body])->asArray()->one();
            $result['body'] = $tmp;
            $return_result['body_wear'] = $tmp['body'];
            $return_result['left_arm'] = $tmp['left_arm'];
            $return_result['right_arm'] = $tmp['right_arm'];
        }

        if(isset($set->left_leg))
        {
            $tmp = Link::find()->where(['item_id' => $set->left_leg])->asArray()->one();
            $result['left_leg'] = $tmp;
            if(isset($tmp['body']))
                $return_result['body_pants'] = $tmp['body'];
            if(isset($tmp['left_leg'])  )
                $return_result['left_leg'] = $tmp['left_leg'];
            if(isset($tmp['right_leg']))
                $return_result['right_leg'] = $tmp['right_leg'];
        }

        if(isset($set->right_leg))
        {
            $tmp = Link::find()->where(['item_id' => $set->right_leg])->asArray()->one();
            $result['right_leg'] = $tmp;
            if(isset($tmp['body']))
                $return_result['body_pants'] = $tmp['body'];
            if(isset($tmp['left_leg'])  )
                $return_result['left_foot'] = $tmp['left_leg'];
            if(isset($tmp['right_leg']))
                $return_result['right_foot'] = $tmp['right_leg'];
        }

        if(isset($set->right_arm))
        {
            $tmp = Link::find()->where(['item_id' => $set->right_arm])->asArray()->one();
            $result['right_arm'] = $tmp;
            if(isset($tmp['left_hand'])  )
                $return_result['left_hand'] = $tmp['left_hand'];
            if(isset($tmp['right_hand']))
                $return_result['right_hand'] = $tmp['right_hand'];
        }

        if(isset($set->left_arm))
        {
            $tmp = Link::find()->where(['item_id' => $set->left_arm])->asArray()->one();
            $result['left_arm'] = $tmp;
            if(isset($tmp['left_hand']) ) 
                $return_result['left_hand'] = $tmp['left_hand'];
            if(isset($tmp['right_hand']))
                $return_result['right_hand'] = $tmp['right_hand'];
        }

        return $return_result;
    }

}