<?php

namespace micro\controllers;

use yii;
use micro\models\Achievement;
use micro\models\Transaction;
use micro\models\Product;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\filters\auth\HttpBearerAuth;

/**
 * Работа с транзакциями
 *
 * Class AchievementController
 * @package micro\controllers
 */
class AchievementController extends ActiveController
{
	public $modelClass = 'micro\models\Transaction';

	public function behaviors()
	{
		// удаляем rateLimiter, требуется для аутентификации пользователя
		$behaviors = parent::behaviors();

		unset($behaviors['rateLimiter']);

		// Возвращает результаты экшенов в формате JSON
		$behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
		// OAuth 2.0
		$behaviors['authenticator'] = ['class' => HttpBearerAuth::className()];

		return $behaviors;
	}

	public function actionGet()
	{
		$result = Achievement::find()->orderBy(['id' => SORT_DESC])->andWhere(['user_id' => Yii::$app->user->id])->all();

        foreach ($result as $key => $r) {
            $end_date = date("Y-m-d H:i:s", strtotime($r->end_date));
            $transactions = Transaction::find()
            ->andWhere(['user_id' => Yii::$app->user->id])
            ->andWhere(['>=', 'date', date("Y-m-d H:i:s", strtotime("-1 weak", strtotime($end_date)))])
            ->all();
            $counter = 0;
            foreach ($transactions as $key => $t) {
                $products = Product::find()->andWhere(['id_trans' => $t->id])->andWhere(['name' => $r->product])->count();
                $counter+= $products;
            }
            $r->bought = $counter;
            $r->save();

            if($r->complited == 0 && $r->needed <= $r->bought)
            {

                $r->complited = 1;
                $r->end_date = date("Y-m-d H:i:s", strtotime($r->end_date));
                $r->save();
                $user = Yii::$app->user->identity;
                $user->balance += $r->award;
                $user->save();
            }    
        }
        return $result;

	}

	public function actionRelevant($user_id) //  получение часто повторяющихся покупок в течении месяца
    {
//        $request = Yii::$app->request;
//
//        $date = $request->post('date');

        $date = date_create('2019-11-23'); // для примера создана своя дата, а не получена постом
        $end = date_create('2019-12-23');
        $date->modify('-1 month');
        $date->format('Y-m-d');

        $end = date_format($end, 'Y-m-d');
        $date = date_format($date, 'Y-m-d');

        // создать модельку продукты

        $product = Product::find()
            ->select('product.*')
            ->leftJoin('transaction', '`transaction`.`id` = `product`.`id_trans`')
            ->where(['>', 'transaction.date', $date])
//            ->groupBy('name')
            ->all();;

        $arrayRelevant = [];
        foreach ($product as $value) {
            if (!isset($arrayRelevant[$value['name']])) {
                $arrayRelevant[$value['name']] = 1;
            } else {
                $arrayRelevant[$value['name']]++;
            }
        }

        $counter = 0;
		arsort($arrayRelevant);

		$start_date = strtotime("23-10-2019");
		$end_date = strtotime("23-12-2019");
        foreach ($arrayRelevant as $key => $r) {
            if($r < 10) continue;
        	$timestamp = mt_rand($start_date, $end_date);
        	$model = new Achievement();
        	$model->user_id = Yii::$app->user->id;
        	$model->product = $key;
        	$model->needed = intval($r/5) * 5;
        	$model->bought = 0;
        	$model->complited = 0;
        	$today = strtotime('+1 months');
        	$model->end_date = date("Y-m-d H:i:s", $timestamp);
        	$model->award = rand(0, 4)*50 + 200;
        	$model->user_id = $user_id;
        	$model->save();
        }
        



        return $arrayRelevant;
    }

}

