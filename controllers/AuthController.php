<?php

namespace micro\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\Response;
use yii\filters\auth\HttpBearerAuth;

use micro\models\User;
use micro\models\UserReg;


/**
 * API для аутентификации пользователя
 *
 * Class AuthController
 * @package micro\controllers
 */
class AuthController extends Controller
{
	public function behaviors()
	{
		// удаляем rateLimiter, требуется для аутентификации пользователя
		$behaviors = parent::behaviors();

		unset($behaviors['rateLimiter']);

		// Возвращает результаты экшенов в формате JSON
		$behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

		return $behaviors;
	}

	/**
	 * Авторизация
	 *
	 * @return string
	 */
	public function actionLogin()
    {
        $request = Yii::$app->request;
        
        $model = User::findIdentityByLogin($request->post('login'));

        if ($model != null) 
        {
            if(password_verify($request->post('password'), $model->password)) 
            {
                if(empty($model->access_token))
                {
                     $model->access_token = uniqid();
                     $model->save();
                }

                return $model->access_token;
            }
            else
            {
                return false;
            }          
        } 
        else 
        {
            return "Not exist.";
        }
    }

	/**
	 * Регистрация
	 *
	 * @return UserReg|string
	 */
	public function actionReg()
    {
        $request = Yii::$app->request;

        $model = User::findIdentityByLogin($request->post('login'));

        if($model != null)
        {
            return "Login used.";
        }
        else
        {
            $model = new UserReg();

            $model->name = $request->post('name');
            $model->login = $request->post('login');
            $model->password = $request->post('password');
            $model->password_repeat = $request->post('password_repeat');
            
            if($model->validate())
            {
                if($model->save())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return "Not validate";
            }
        }
    }

    public function actionGet() {
		return Yii::$app->user->identity;
	}
}